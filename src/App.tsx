import React from 'react'
import './App.css'

function App() {
  const [count, setCount] = React.useState<number>(0);

  const increment = React.useCallback(() => {
      setCount((prevCount) => prevCount + 1)
  }, []);

  return (
    <div>
      <button type="button" onClick={increment}>{count}</button>
        <MyComponent id={count} label="Salut">
            <h1>Titre</h1>
        </MyComponent>
        <p>bonjour</p>
    </div>
  )
}

export default App

type Props = React.PropsWithChildren<{
    id: number;
    label: string;
}>

function MyComponent(props: Props) {
    const { id, label, children } = props;

    return (
        <React.Fragment>
            <p>Mon id: {id} / Mon label : {label}</p>
            {children}
        </React.Fragment>
    )
}
